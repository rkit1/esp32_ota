#!/usr/bin/python


import SimpleHTTPServer
import SocketServer
import os
import sys
import socket
import time
import json

os.chdir("build")

with open('project_description.json') as f:
    data = json.load(f)
    app_bin = data["app_bin"]

with open('config/sdkconfig.json') as f:
    data = json.load(f)
    http_port = data["RKIT_OTA_HTTP_PORT"]
    udp_port = data["RKIT_OTA_UDP_PORT"]

class MyTCPServer(SocketServer.TCPServer):
    def handle_timeout(self):
        print "timeout"
        sys.exit(-1)


upload_host = sys.argv[1]

message = str(http_port) + "\t/" + app_bin
print message

httpd = MyTCPServer(("", int(http_port)), SimpleHTTPServer.SimpleHTTPRequestHandler)
socket.socket(socket.AF_INET, socket.SOCK_DGRAM).sendto(message, (upload_host, int(udp_port)))


httpd.timeout = 5
httpd.handle_request()
time.sleep(30);
print "success"


