#include "OTA.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "string.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"

#define PORT CONFIG_RKIT_OTA_UDP_PORT

static const char *TAG = "rkit_OTA";

static void print_app_info() {
  const esp_app_desc_t * desc = esp_ota_get_app_description(); // Возвращает неправильное значение
  const esp_partition_t * part = esp_ota_get_running_partition();
  ESP_LOGI(TAG, "app build %s %s, partition label: %s", desc->date, desc->time, part->label);
}

static void ota_task(void *pvParameter)
{
  print_app_info();
  char rx_buffer[32];
  char addr_str[32];

  
  struct sockaddr_in dest_addr = {};
  dest_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  dest_addr.sin_family = AF_INET;
  dest_addr.sin_port = htons(PORT);
  int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
  struct sockaddr_in6 source_addr;
  socklen_t socklen = sizeof(source_addr);

  while(true) {
    int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&source_addr, &socklen);
    if (len < 0) {
      if(errno != EWOULDBLOCK) ESP_LOGE(TAG, "recvfrom failed: errno %d", errno);
      vTaskDelay(1000);
      continue;
    } else {
      if (source_addr.sin6_family == PF_INET) {
        inet_ntoa_r(((struct sockaddr_in *)&source_addr)->sin_addr.s_addr, addr_str, sizeof(addr_str) - 1);
      } else if (source_addr.sin6_family == PF_INET6) {
        inet6_ntoa_r(source_addr.sin6_addr, addr_str, sizeof(addr_str) - 1);
      }

    }
    rx_buffer[len] = 0;
    ESP_LOGI(TAG, "Received %d bytes from %s:", len, addr_str);
    ESP_LOGI(TAG, "%s", rx_buffer);


    int port;
    char path[32];
    if (sscanf(rx_buffer, "%d\t%s", &port, path) == 2) {
      ESP_LOGI(TAG, "packet ok");
      esp_ota_mark_app_valid_cancel_rollback();
      esp_http_client_config_t config = {};
      config.host = addr_str;
      config.port = port;
      config.path = path;
      config.event_handler = NULL;
      esp_err_t ret = esp_https_ota(&config);
      if (ret == ESP_OK) {
        esp_restart();
      } else {
        ESP_LOGE(TAG, "Firmware upgrade failed");
      }
    }
  }
}

void rkit_ota_init() {
  xTaskCreate(&ota_task, "ota_task", 8192, NULL, 0, NULL);
  ESP_LOGI(TAG, "task created");
}
